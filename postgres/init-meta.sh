#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER docker;
    CREATE DATABASE docker;
    GRANT ALL PRIVILEGES ON DATABASE docker TO docker;

    CREATE TABLE "airtable_raw"
    (
     id   	 serial NOT NULL UNIQUE,
     launch_date TIMESTAMPTZ NOT NULL DEFAULT NOW(),
     raw_data    text NOT NULL
    );
EOSQL
