from django.db import models


class Method(models.Model):
    name = models.CharField(max_length=30, unique=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class Therapist(models.Model):
    name = models.CharField(max_length=30)
    photo = models.CharField(max_length=150)
    airtable_id = models.CharField(max_length=100, unique=True)
    methods = models.ManyToManyField(Method, blank=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name
