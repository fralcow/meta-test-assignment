from django.contrib import admin
from .models import Therapist, Method

admin.site.register(Therapist)
admin.site.register(Method)
