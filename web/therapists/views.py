from django.shortcuts import get_object_or_404, render
from .models import Therapist


def index(request):
    therapist_list = Therapist.objects.order_by("name")
    context = {
        "therapist_list": therapist_list,
    }
    return render(request, "therapists/index.html", context)


def detail(request, therapist_id):
    therapist = get_object_or_404(Therapist, pk=therapist_id)
    methods = therapist.methods.all()
    photo = therapist.photo
    return render(
        request,
        "therapists/detail.html",
        {
            "therapist": therapist,
            "methods": methods,
            "photo": photo,
        },
    )
