from airtable import Airtable
import os
import json
from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, Table, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.sql import func
from sqlalchemy.dialects.postgresql import insert
import argparse

api_key = os.environ.get("AIRTABLE_TOKEN", None)
baseKey = os.environ.get("AIRTABLE_BASEKEY", None)
db_username = os.environ.get("POSTGRES_USER")
db_password = os.environ.get("POSTGRES_PASSWORD")
db_name = os.environ.get("POSTGRES_DB")
db_port = os.environ.get("DB_PORT")
db_service = os.environ.get("DB_SERVICE")


Base = declarative_base()

association_table = Table(
    "therapists_therapist_methods",
    Base.metadata,
    Column("therapist_id", Integer, ForeignKey("therapists_therapist.id")),
    Column("method_id", Integer, ForeignKey("therapists_method.id")),
)


class Therapist(Base):
    __tablename__ = "therapists_therapist"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    airtable_id = Column(String)
    photo = Column(String)
    methods = relationship(
        "Method", secondary=association_table, back_populates="therapists"
    )

    def __repr__(self):
        return "<Therapist(name='%s')>" % (self.name)


class Method(Base):
    __tablename__ = "therapists_method"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    therapists = relationship(
        "Therapist", secondary=association_table, back_populates="methods"
    )

    def __repr__(self):
        return "<Method(name='%s')>" % (self.name)


class RawData(Base):
    __tablename__ = "airtable_raw"

    id = Column(Integer, primary_key=True)
    launch_date = Column(DateTime(timezone=True), default=func.now())
    raw_data = Column(String)


def download_data(tableName, grid):
    """Downloads data from airtable
    :returns: list

    """
    airtable = Airtable(baseKey, tableName, api_key)

    data = airtable.get_all(view=grid, maxRecords=50)
    return data


def commit_raw_data(session, data):
    data_string = json.dumps(data)
    data_db = RawData(raw_data=data_string)
    session.add(data_db)


def commit_therapists(session, engine, data):
    """Commits therapists to db

    :data: response from airtable
    :returns: None
    """

    therapists = []
    for d in data:
        t = {
            "name": d["fields"]["Имя"],
            "airtable_id": d["id"],
            "photo": d["fields"]["Фотография"][0]["thumbnails"]["large"][
                "url"
            ],
        }
        therapists.append(t)

    # delete therapists not in the airtable
    db_therapists = session.query(Therapist).all()
    for t in db_therapists:
        if t.airtable_id not in [x["airtable_id"] for x in therapists]:
            session.delete(t)

    # insert or update therapists from the airtable
    Therapist().metadata.create_all(engine)
    meta = MetaData(engine)
    meta.reflect()
    table = Table("therapists_therapist", meta, autoload=True)
    stmt = insert(Therapist).values(therapists)
    stmt = stmt.on_conflict_do_update(
        index_elements=[table.c["airtable_id"]],
        set_={"name": stmt.excluded.name, "photo": stmt.excluded.photo},
    )
    session.execute(stmt)


def get_methods_for_therapist(session, methods):
    """Gets database ids for methods"""
    method_ids = []
    for m in methods:
        q = session.query(Method).filter(Method.name == m)
        method_id = q.first()
        method_ids.append(method_id)
    return method_ids


def commit_methods(session, engine, data):
    """Commits methods to db

    :data: response from airtable
    :returns: None

    """
    all_methods = []
    for d in data:
        all_methods.append(d["fields"]["Методы"])

    # flatten the list
    all_methods = [item for sublist in all_methods for item in sublist]
    unique_methods = set(all_methods)

    methods = []
    for m in unique_methods:
        methods.append({"name": m})

    # delete methods not in the airtable
    db_methods = session.query(Method).all()
    for m in db_methods:
        if m.name not in unique_methods:
            session.delete(m)

    # insert or update methods from the airtable
    Method().metadata.create_all(engine)
    meta = MetaData(engine)
    meta.reflect()
    table = Table("therapists_method", meta, autoload=True)
    stmt = insert(Method).values(methods)
    stmt = stmt.on_conflict_do_nothing(
        index_elements=[table.c["name"]],
    )
    session.execute(stmt)


def commit_therapists_methods(session, engine, data):
    """Commits data to therapists_therapist_method table"""
    for d in data:
        methods = d["fields"]["Методы"]
        t_methods = get_methods_for_therapist(session, methods)
        t = session.query(Therapist).filter_by(airtable_id=d["id"]).first()
        for m in t_methods:
            t.methods.append(m)


def main():
    parser = argparse.ArgumentParser(description="Process some integers.")
    parser.add_argument(
        "-t",
        "--table",
        type=str,
        nargs=1,
        required=True,
        help="Table name",
    )
    parser.add_argument(
        "-g",
        "--grid",
        type=str,
        nargs="+",
        required=True,
        help="Grid name (the one just below the table name in Airtable)",
    )

    args = parser.parse_args()
    data = download_data(args.table[0], " ".join(args.grid))
    engine = create_engine(
        f"postgresql+psycopg2://{db_username}:{db_password}@{db_service}:"
        f"{db_port}/{db_name}"
    )
    Session = sessionmaker(bind=engine)
    session = Session()
    try:
        commit_raw_data(session, data)
        commit_methods(session, engine, data)
        commit_therapists(session, engine, data)
        commit_therapists_methods(session, engine, data)

        session.commit()
    except SQLAlchemyError:
        session.rollback()
        raise
    finally:
        session.close()
    print("Success")


if __name__ == "__main__":
    main()
