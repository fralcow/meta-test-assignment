# Установка и запуск
## Макет сайта
1. Создать `.env` по образцу `.env.sample` и ввести туда параметры [AIRTABLE_TOKEN](https://support.airtable.com/hc/en-us/articles/219046777-How-do-I-get-my-API-key-) и [AIRTABLE_BASEKEY](https://airtable.com/api). Остальные параметры можно оставить как в образце.
2. (Опционально) `docker-compose down && docker volume rm meta-test-assignment_pgdata` удаляет volume базы данных, этот шаг нужен для чистого перезапуска докера.
3. `docker-compose run postgres`, дождаться инициализации базы данных (только при первом чистом запуске), `CTRL+C`.
4. `docker-compose up -d --build` запустить контейнеры.
5. `docker-compose exec web python manage.py migrate` применить миграции django.
6. Открыть `http:127.0.0.1:8000` в браузере

## Загрузка из airtable
1. `cd downloader`
2. `pipenv install` чтобы установить зависимости для виртаульной среды.
3. `PIPENV_DOTENV_LOCATION=../.env pipenv shell` запустить виртуальную shell с привязкой к файлу, содержащему environment variables.
4. `python donwload.py -t TABLE -g GRID` для запуска скрипта по скачиванию данных в базу данных postgres, поднятую на докере. `-t` -- название таблицы в Airtable (Psychotherapists), `-g` -- Grid в Airtable (Grid view)

# Optional
- When `web` is running, `docker-compose exec web python manage.py createsuperuser` to create an admin
